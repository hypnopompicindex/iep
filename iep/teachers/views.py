from teachers.models import Teacher
from teachers.serializers import TeacherSerializer
from rest_framework import generics


class TeacherListCreate(generics.ListCreateAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
