from django.urls import path
from . import views

urlpatterns = [
    path('api/teacher/', views.TeacherListCreate.as_view()),
]
