import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

export default function Header() {
  return (
    <div>
      <MuiThemeProvider>
        <Typography>body1</Typography>
        <h1>Header</h1>
      </MuiThemeProvider>
    </div>
  );
}
