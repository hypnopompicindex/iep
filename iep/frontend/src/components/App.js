import React from "react";
import ReactDOM from "react-dom";
import DataProvider from "./DataProvider";
import Table from "./Table";
import Form from "./Form";
import Dashboard from "./layout/Dashboard";

const App = () => (
  <React.Fragment>
    <Dashboard />
    <DataProvider
      endpoint="api/teacher/"
      render={data => <Table data={data} />}
    />
    <Form endpoint="api/teacher/" />
  </React.Fragment>
);

const wrapper = document.getElementById("app");

wrapper ? ReactDOM.render(<App />, wrapper) : null;
